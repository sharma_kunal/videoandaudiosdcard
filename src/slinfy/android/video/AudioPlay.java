package slinfy.android.video;

import java.io.IOException;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;

public class AudioPlay extends Activity {

	private Button buttonPlayStop, buttonPause;
	private MediaPlayer mediaPlayer;
	boolean isplying = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.audio_paly);

		buttonPlayStop = (Button) findViewById(R.id.ButtonPlayStop);
		buttonPause = (Button) findViewById(R.id.ButtonPause);

		final String data = getIntent().getStringExtra("mp3name");
		buttonPlayStop.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				mediaPlayer = new MediaPlayer();
				try {
					mediaPlayer.setDataSource(data);
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalStateException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
				try {
					if (!isplying) {
						isplying = true;
						mediaPlayer.prepare();
						mediaPlayer.start();
					} else {
						mediaPlayer.start();
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
		buttonPause.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub
				// mediaPlayer = new MediaPlayer();
				try {
					// mediaPlayer.prepare();
					mediaPlayer.pause();

				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});

	}

	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}
}
