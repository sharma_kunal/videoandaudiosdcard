package slinfy.android.video;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoPlayerOnline2Activity extends Activity {

	VideoView vview;
	String urlsource = "http://192.168.1.92/Stream/Wildlife.mp4";
	MediaController controller;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		vview = (VideoView) findViewById(R.id.vview);
		try {
			setsource(urlsource);
		} catch (Exception e) {

		}

	}

	public void setsource(String source) throws IOException {
		URL url = new URL(source);
		URLConnection cn = url.openConnection();
		cn.connect();

		Uri uri = Uri.parse("http://192.168.1.92/Stream/a.wmv"); // i.e. mp4
																	// version
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(uri, "video/*"); // important! otherwise you just
												// download the video
		startActivity(intent);

		vview.setVideoPath(source);
		controller = new MediaController(this);
		controller.setMediaPlayer(vview);
		vview.setMediaController(controller);

		vview.requestFocus();
		vview.start();
		System.out.println("VideoOnline.setsource()"
				+ vview.getMeasuredHeight());
		System.out
				.println("VideoOnline.setsource()" + vview.getMeasuredWidth());
	}

}